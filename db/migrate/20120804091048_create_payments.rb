class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :sum
      t.integer :user_id
      t.boolean :approved

      t.timestamps
    end
  end
end
