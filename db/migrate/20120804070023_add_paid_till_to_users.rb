class AddPaidTillToUsers < ActiveRecord::Migration
  def change
    add_column :users, :paid_till, :date, :null => true

  end
end
