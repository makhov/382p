var questionForm = {

    questions : [],
    index : 0,
    tooltips: {
        6: {0:"порядок применения организационных мер защиты информации или использования технических средств защиты информации не определен во внутренних документах оператора по переводу денежных средств, оператора платежных систем, оператора услуг платежной инфраструктуры",
            1:"порядок применения организационных мер защиты информации или использования технических средств защиты информации определен во внутренних документах оператора по переводу денежных средств, оператора платежных систем, оператора услуг платежной инфраструктуры, но соответствующие организационные меры защиты информации не применяются, или технические средства защиты информации не используются",
            2:"порядок применения организационных мер защиты \
            информации или использования технических средств защиты \
            информации определен во внутренних документах оператора по \
            переводу денежных средств, оператора платежных систем, оператора \
            услуг платежной инфраструктуры, но применение соответствующих \
            организационных мер защиты информации или использование \
            технических средств защиты информации осуществляется не в полном\
            соответствии с указанным порядком",
            3:"порядок применения организационных мер защиты\
            информации или использования технических средств защиты\
            информации определен во внутренних документах оператора по\
            переводу денежных средств, оператора платежных систем, оператора\
            услуг платежной инфраструктуры, но применение соответствующих\
            организационных мер защиты информации или использование\
            технических средств защиты информации осуществляется почти в\
            полном соответствии с указанным порядком",
            4:"порядок применения организационных мер защиты\
            информации или использования технических средств защиты\
            информации определен во внутренних документах оператора по\
            переводу денежных средств, оператора платежных систем, оператора\
            услуг платежной инфраструктуры, и применение соответствующих\
            организационных мер защиты информации и использование\
            технических средств защиты информации осуществляется в полном\
            соответствии с указанным порядком",
            5:"выполнение требования не является обязанностью для\
            данного субъекта платежной системы"
        },
        4: {0: "деятельность не выполняется",
            1: "деятельность выполняется частично",
            2: "деятельность выполняется полностью",
            3: "выполнение требования не является обязанностью для данного субъекта платежной системы"
        },
        3: {0: "документ отсутствует",
            1: "документ имеется в наличии",
            2: "выполнение требования не является обязанностью для данного субъекта платежной системы"
        }
    },

    init: function() {
        var that = this;
        that.questions = $('.control-group:not(.last)');
        that.showQuestion();
        $('.nav-prev').addClass('disabled');
        $('.generate-submit').hide();
        for (var i = 0; i < 129; i++) {
            $(that.questions[i]).append('<div style="display: none" class="control-group area_'+i+'"><label class="control-label">Краткое обоснование выставленной оценки</label><div class="controls"><textarea class="input-xlarge" name="description['+(i+1)+']" rows="3" style="width:100%;"></textarea></div></div>');
    }

        $('.radio input').change(function () {
            that.showDescription();
        })

        $('.nav-next').click( function() {
            if( ! $(this).hasClass('disabled') ) {
                that.showNextQuestion();
            }
        });
        $('.nav-prev').click( function() {
            if( ! $(this).hasClass('disabled') ) {
                that.showPrevQuestion();
            }
        });
    },

    showQuestion: function() {
        $('.control-group').hide();
        $('.question-number').text(this.index+1);
        $(this.questions[this.index]).show();
        this.showTooltips();
        this.updateProgress();
        $('.nav-next').removeClass('disabled');
        $('.nav-prev').removeClass('disabled');
        $('.generate-submit').hide();
    },

    showNextQuestion: function() {
        this.index += 1;
        if( ( this.index ) >= questionForm.questions.size() ) {
            $('.question-number').text(this.index+1);
            $('.control-group').hide();
            $('.nav-next').addClass('disabled');
            $('.company-info').children().show();
            $('.bar').css('width','100%');
        }
        else {
            this.showQuestion();
        }
    },

    showPrevQuestion: function() {
        this.index -= 1;
        if( this.index >= 0 ) {
            this.showQuestion();
            if( this.index == 0 ) {
                $('.nav-prev').addClass('disabled');
            }
        }
    },
    showQuestionByNum: function( Num ) {
        this.index = Num - 1;
        if( this.index >= 0 ) {
            this.showQuestion();
            if( this.index == 0 ) {
                $('.nav-prev').addClass('disabled');
            }
        }
    },

    showTooltips: function() {
        var labels = $(this.questions[this.index]).children('div').children('label');
        for ( var tooltipId in this.tooltips[labels.length-1] ) {
            $(labels[tooltipId]).append('&nbsp;<a href="#" rel="tooltip" title="'+this.tooltips[labels.length-1][tooltipId]+'"><span class="icon-question-sign"></span></a>')
        }
    },
    updateProgress: function() {
        var percent = ( 95 / 129 ) * ( this.index + 1 );
        $('.bar').css('width',percent+'%');
    },
    showDescription: function() {
        var question = $('input[name="q['+(this.index+1)+']"]:checked');
        $('.area_'+this.index).hide();
        if( question.val() != 'н/о' ) {
            $('.area_'+this.index).show();
        }
        else {
            question.children('.area').remove();
        }
    }
}

$(document).ready(function () {
    questionForm.init();
})