class Payment < ActiveRecord::Base
  belongs_to :user

  def approve!
    self.approved = true
    self.save
  end
end
