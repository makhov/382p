module PagesHelper
  def ss_title
    base_title = I18n.t(:base_title)
    if defined?(@page.title).nil?
      base_title
    else
      "#{@page.title} | #{base_title}"
    end
  end
  def ss_keywords
    base_keywords = I18n.t(:base_keywords)
    if defined?(@page.keywords).nil?
      base_keywords
    else
      @page.keywords
    end
  end
  def ss_description
    base_description = I18n.t(:base_description)
    if defined?(@page.description).nil?
      base_description
    else
      @page.description
    end
  end
end
