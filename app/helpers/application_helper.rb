module ApplicationHelper

  def ss_title
    base_title = t(:base_title)
    if defined?(@title).nil?
      base_title
    else
      "#{base_title} | #{@title}"
    end
  end
  def ss_keywords
    base_keywords = t(:base_keywords)
    if defined?(@keywords).nil?
      base_keywords
    else
      "#{base_keywords} | #{@keywords}"
    end
  end
  def ss_description
    base_description = t(:base_description)
    if defined?(@description).nil?
      base_description
    else
      "#{base_description} | #{@description}"
    end
  end

end
