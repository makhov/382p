class AccountController < ApplicationController

  layout "dashboard"

  before_filter :require_login, :require_to_pay
  skip_before_filter :require_to_pay, :only => :pay

  def pay
    @payment = Payment.new( :user => current_user, :sum => 100 )
    @payment.save
  end

  def index

  end

  def upload
    uploaded_io = params[:answers]
    filename = "docx/users/#{current_user.id}/answers_#{Time.now.to_i}.yaml"
    File.open( filename, 'w') do |file|
      file.write(uploaded_io.read)
    end
    data = YAML.load_file( filename )
    abort(data.to_yaml)
  end

end
