class ApplicationController < ActionController::Base
  protect_from_forgery
  include ActiveMerchant::Billing::Integrations

  def after_sign_in_path_for(resource)
    dashboard_index_path # <- Path you want to redirect the user to.
  end

  def after_sign_up_path_for(resource)
    dashboard_index_path # <- Path you want to redirect the user to after signup
  end

  def require_login
    unless current_user
      redirect_to new_user_session_path
    end
  end

  def require_to_pay
    unless current_user.is_paid
      redirect_to account_pay_path
    end
  end

end
