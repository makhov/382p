class PagesController < ApplicationController
  layout "application"

  def index
    @page = Page.new( :title => nil, :keywords => nil, :description => nil )
  end

  def about
    @page = Page.new( :title => nil, :keywords => nil, :description => nil )
  end

  def show
    @page = Page.find_by_url( params[:url] )
  end

end
