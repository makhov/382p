# coding: utf-8
class GenerateController < ApplicationController

  require 'zip/zip'
  require 'nokogiri'

  def download
    send_file("#{Rails.root}/docx/users/#{current_user.id}/output.zip", :filename => "352.zip")
  end

  def do

    for i in 1..129
      if params[:q]["#{i}"].nil?
        params[:q]["#{i}"] = 'н/о'
      end

      if params[:description]["#{i}"].nil?
        params[:description]["#{i}"] = ""
      end
    end

    filename = "#{Rails.root}/docx/questions.yml"
    questionList = YAML.load_file( filename )

    k1NullCount = 0
    ev1Sum = 0
    ev1NoneCount = 0
    for i in 1..81
      if ( params[:q]["#{i}"] == '0' )
        k1NullCount += 1
      end

      if( params[:q]["#{i}"] == 'н/о' )
        ev1NoneCount += 1
      else
        ev1Sum += params[:q]["#{i}"].to_f
      end
    end

    if( k1NullCount == 0 )
      k1 = 1
    else if ( k1NullCount < 11 )
        k1 = 0.85
      else
        k1 = 0.7
      end
    end

    k2NullCount = 0
    ev2Sum = 0
    ev2NoneCount = 0
    for i in 82..129
      if ( params[:q]["#{i}"] == '0' )
        k2NullCount += 1
      end

      if( params[:q]["#{i}"] == 'н/о' )
        ev2NoneCount += 1
      else
        ev2Sum += params[:q]["#{i}"].to_f
      end
    end

    if( k2NullCount == 0 )
      k2 = 1
    else if ( k2NullCount < 6 )
           k2 = 0.85
         else
           k2 = 0.7
         end
    end

    ev1Val = (( ev1Sum * k1 ) / ( 81 - ev1NoneCount )).round(2)
    ev2Val = (( ev2Sum * k2 ) / ( 48 - ev2NoneCount )).round(2)
    rVal = [ev1Val,ev2Val].min

    directory_name = "#{Rails.root}/docx/users/#{current_user.id}"
    Dir.mkdir(directory_name) unless File.exists?(directory_name)
    output_directory_name = "#{Rails.root}/docx/users/#{current_user.id}/output"
    Dir.mkdir(output_directory_name) unless File.exists?(output_directory_name)

    #FileUtils.cp 'docx/template.docx', "#{directory_name}/exam.docx"
    FileUtils.rm_rf "#{directory_name}/exam"
    FileUtils.cp_r 'docx/template', "#{directory_name}/exam"

    #zip = Zip::ZipFile.open("#{directory_name}/exam.docx")
    #doc = zip.find_entry("word/document.xml")
    xml = Nokogiri::XML(File.open("#{directory_name}/exam/word/document.xml"))
    #xml = Nokogiri::XML.parse(doc.get_input_stream)

    ceo = xml.xpath('//w:p[@w:rsidR="001A34FD"][@w:rsidRPr="000B41E1"]/w:r/w:t').first
    ceo.content = params[:ceo_post]

    ceo_name = xml.xpath('//w:p[@w:rsidR="00E151D3"][@w:rsidRPr="0046369A"][@w:rsidRDefault="000B41E1"]/w:r/w:t')
    ceo_name = ceo_name[1]
    ceo_name.content = params[:ceo_name]

    dateStr = self.dateIt()
    date1 = xml.xpath('//w:p[@w:rsidR="00E151D3"][@w:rsidRPr="00616E05"][@w:rsidRDefault="00616E05"]/w:r/w:t')
    date1[0].content = ''
    date1 = date1[1]  # в date[0] лежит «%»
    date1.content = dateStr
    date2 = xml.xpath('//w:p[@w:rsidR="001A34FD"][@w:rsidRPr="0046369A"][@w:rsidRDefault="00616E05"]/w:r/w:t')
    date2[0].content = ''
    date2 = date2[1]  # в date2[0] лежит «%»
    date2.content = dateStr

    company_name_1 = xml.xpath('//w:p[@w:rsidR="00E151D3"][@w:rsidRPr="0046369A"][@w:rsidRDefault="00286B50"]/w:r/w:t').first
    company_name_1.content = params[:company_name]

    company_name_2 = xml.xpath('//w:r[@w:rsidR="00286B50"][@w:rsidRPr="0046369A"]/w:t')
    company_name_2 = company_name_2[3]
    company_name_2.content = params[:company_name]

    total = xml.xpath('//w:p[@w:rsidR="0046369A"][@w:rsidRPr="0046369A"][@w:rsidRDefault="0046369A"][@w:rsidP="00BF099F"]/w:r/w:t')
    ev1 = total[1] # в total[0] и total[2] лежит «%»
    total[0].content = ''
    total[2].content = ''
    ev1.content = ev1Val.to_s
    ev2 = total[4] # в total[3] и total[5] лежит «%»
    total[3].content = ''
    total[5].content = ''
    ev2.content = ev2Val.to_s
    r = total[7] # в total[6] и total[8] лежит «%»
    total[6].content = ''
    total[8].content = ''
    r.content = rVal.to_s

    auditor = xml.xpath('//w:p[@w:rsidR="001A34FD"][@w:rsidRPr="0046369A"][@w:rsidRDefault="001A34FD"][@w:rsidP="00BF099F"]/w:r/w:t')
    auditor[0].content = ''
    auditor[2].content = ''
    auditor = auditor[1] # в auditor[0] и auditor[2] лежит «%»
    auditor.content = params[:auditor_name]

    tr = xml.xpath('//w:tr[@w:rsidR="008E3A05"][@w:rsidRPr="0046369A"][@w:rsidTr="008E3A05"]')

    trXmlString = tr[1].inner_html
    outputTrXmlString = ''

    tr[1].inner_html = tr[1].inner_html.gsub('%REQUIREMENT%', questionList[1]).gsub('%EVALUATION%', params[:q]["1"]).gsub('%FOUNDATION%', params[:description]["1"])
    for i in 2..129
      outputTrXmlString += '<w:tr w:rsidR="008E3A05" w:rsidRPr="0046369A" w:rsidTr="008E3A05"><w:trPr><w:cantSplit/></w:trPr>'
      outputTrXmlString += trXmlString.gsub('%REQUIREMENT%', questionList[i]).gsub('%EVALUATION%', params[:q]["#{i}"]).gsub('%FOUNDATION%', params[:description]["#{i}"])
      outputTrXmlString += '</w:tr>'
    end

    tr[1].parent.inner_html += outputTrXmlString

    File.open("#{directory_name}/exam/word/document.xml", "w+") do |f|
      f.write(xml.to_s)
    end
    #zip.get_output_stream("word/document.xml") { |f| f << xml.to_s }
    #zip.close
    #FileUtils.rm "#{directory_name}/exam.docx"

    self.compress("#{directory_name}/exam/", 'docx')
    #self.generate_yaml()
    FileUtils.cp "#{directory_name}/exam.docx", "#{directory_name}/output/exam.docx"
    #FileUtils.cp "#{directory_name}/answers.yaml", "#{directory_name}/output/answers.yaml"
    self.compress("#{directory_name}/output/")
  end

  protected

    def generate_yaml()
      questions = []
      for i in 1..129
        questions[i] = { 'question' => params[:q][i], 'description' => params[:description][i] }
      end
      filename = "#{Rails.root}docx/users/#{current_user.id}/answers.yaml"
      File.open( filename, 'w' ) do |out|
        YAML.dump( questions, out )
      end
      text=''
      File.open(filename,"r"){ |f| f.gets; text=f.read}
      File.open(filename,"w+"){ |f| f.write(text)  }

    end

    def read_yaml()
      filename = "docx/users/#{current_user.id}/answers.yaml"
      data = YAML.load_file( filename )
      Rails.logger.debug "asdfas #{data}"
      abort(data.to_yaml)
    end

    def dateIt()
      date = Date.today
      monthes = {'01' => 'января','02' => 'февраля','03' => 'марта','04' => 'апреля','05' => 'мая','06' => 'июня','07' => 'июля','08' => 'августа','09' => 'сентября','10' => 'октября','11' => 'ноября','12' => 'декабря'}
      Date::DATE_FORMATS[:custom_month] = "%m"
      month = monthes[date.to_formatted_s(:custom_month)]
      Date::DATE_FORMATS[:day] = "%d"
      Date::DATE_FORMATS[:year] = "%Y"
      return date.to_formatted_s(:day)+" #{month} "+date.to_formatted_s(:year)
    end
    def compress(path, filetype = 'zip')
      require 'zip/zipfilesystem'

      path.sub!(%r[/$],'')
      archive = File.join(path+'/../',File.basename(path))+".#{filetype}"
      FileUtils.rm archive, :force=>true
      fileArr = []
      Zip::ZipFile.open(archive, 'w') do |zipfile|
        Dir["#{path}/**/**"].reject{|f|f==archive}.each do |file|
          fileArr.push(file)
          zipfile.add(file.sub(path+'/',''),file)
        end
        # Хак для файла .rels
        if filetype == 'docx'
          relsFile = path+'/_rels/.rels'
          if ! fileArr.include?(relsFile)
            zipfile.add(relsFile.sub(path+'/',''),relsFile)
          end
        end
      end
    end
end
